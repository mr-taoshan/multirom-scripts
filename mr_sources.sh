#!/bin/bash

cd omni
rm -rf bootable/recovery

git clone https://bitbucket.org/mr-taoshan/multirom-twrp.git bootable/recovery

git clone https://bitbucket.org/mr-taoshan/multirom-sources.git system/extras/multirom

git clone https://bitbucket.org/mr-taoshan/multirom-taoshan.git device/sony/taoshan

git clone https://github.com/Tasssadar/libbootimg.git system/extras/libbootimg

git clone https://github.com/varunchitre15/proprietary_vendor_sony.git -b cm-11.0 vendor/sony

git clone https://github.com/CyanogenMod/android_device_sony_common.git -b cm-11.0 device/sony/common

git clone https://github.com/CyanogenMod/android_device_sony_qcom-common.git -b cm-11.0 device/sony/qcom-common

